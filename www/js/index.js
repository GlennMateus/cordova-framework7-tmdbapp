// Theme selection (theme=".." at <html> opening tag

var theme = 'auto';
if (document.location.search.indexOf('theme=') >= 0) {
    theme = document.location.search.split('theme=')[1].split('&')[0];
}

//Reference for searchbar at search page

var searchbar = null;

//Flag to check our network connection status
var isConnected = false;

//Framework7's initialization
var app = new Framework7({
    id: 'br.ufpr.sept.ti026.tmdbclient',
    name: "The Movie Database",
    root: '#app',
    theme: theme,
    init: true,
    initOnDeviceReady: true,
    touch: {
        tapHold: true,
        materialRipple: true,
        fastClicks: true
    },
    routes: [
        {
            name: "homepage",
            path: "/homepage",
            url: "./pages/homepage.html",
            on: {
                pageAfterIn: function (e) {
                    var $$ = app.$;
                    $$('.reloadlink').on('click', function (e) {
                        if (!isConnected) {
                            app.dialog.alert("No network connection available, try again later.");
                        } else {
                            var id = $$('.tab-active').attr('id');
                            content_cache['#' + id] = null;
                            tmdb_request(id, '#' + id);
                        }
                    });

                    $$('div.tab').on('tab:show', function (e) {

                        if ($$(this).hasClass("tab-active") && $$(this).hasClass("swiper-slide") && !$$(this).hasClass("swiper-slide-active")) {
                            tmdb_request($$(this).attr('id'), '#' + $$(this).attr('id'));
                        }
                    });

                    tmdb_request($$('.tab-active').attr('id'), '#' + $$('.tab-active').attr('id'));
                }
            }
        },
        {
            name: "searchpage",
            path: "/searchpage",
            url: "./pages/searchpage.html",
            on: {
                pageInit: function (e) {
                    searchbar = app.searchbar.create({
                        el: 'form.searchbar',
                        enabled: true,
                        on: {
                            clear: function (e) {
                                app.$('#searchresults').empty();
                            }
                        }
                    });
                },
                pageAfterIn: function (e) {
                    app.$('form.searchbar').on('submit', function (e) {
                        e.preventDefault();
                        tmdb_search(app.$('input[type="search"]').val());
                    });
                }
            }
        }
    ]
});

//Storage for generated lists - being polite to TMDB's API :)
var content_cache = {};

//Same as above, but for single movie's data
var movie_cache = {};

//Template to show movie lists
var movieList = app.t7.compile(app.$('#itemlist').html());

//Template to show movie's Templates (popup)
var movieDetail = app.t7.compile(app.$('#showmovie').html());

//Your API key goes here
var tmdb_apiv3key = 'd8701111771398ec108faba4b87e36b0';

//API endpoint's base URL
var tmdb_apiurl = "https://api.themoviedb.org/3";

//TMDB's configuration (retrieved upon start)
var tmdb_config = null;

/**
 * Generate a 5 star rating based on movie's vote average
 *
 * @param movie
 * @param popup
 * @returns {string}
 */
function tmdb_stars(movie, popup) {
    var v_voted = Math.round(movie.vote_average) / 2;
    var output = '<div>';
    var i = 0;
    for (; i < v_voted; ++i) {
        output += '<i class="material-icons thumbup">star</i>';
    }
    for (; i < 5; ++i) {
        output += '<i class="material-icons">star</i>';
    }
    output += '</div>';
    if (!popup) {
        output += '<a href="#" class="link moviedetail" data-movie="' + movie.id + '">More ...</a>';
    }
    return output;
}

/**
 * Retrieve TMDB configuration
 */
function tmdb_configure() {
    if (tmdb_config == null) {
        var url = tmdb_apiurl + '/configuration?api_key=' + tmdb_apiv3key;
        app.dialog.preloader("Initializing...");
        app.request.json(url, function (data) {
            tmdb_config = data;
            app.dialog.close();
        });
    }
}

/**
 * Retrieve movie's Details
 *
 * @param movieid
 */
function tmdb_moviedetails(movieid) {
    var url = tmdb_apiurl + '/movie/' + movieid + '?api_key=' + tmdb_apiv3key;
    if (typeof movie_cache[movieid] == 'undefined') {
        app.dialog.preloader();
        app.request.json(url, function (data) {
            data.image_path = tmdb_config.images.base_url + tmdb_config.images.poster_sizes[3] + data.backdrop_path;
            data.stars = tmdb_stars(data, true);
            app.dialog.close();
            movie_cache[movieid] = movieDetail(data);
            popup_details(movie_cache[movieid]);
        });
    } else {
        popup_details(movie_cache[movieid]);
    }
}

/**
 * Send request for pages of data (upcoming, top_rated, popular)
 * @param type
 * @param target
 */
function tmdb_request(type, target) {
    if (isConnected) {
        if (typeof content_cache[target] == "undefined" || content_cache[target] == null) {
            var url = tmdb_apiurl + '/movie/' + type + '?language=en-US&page=1&api_key=' + tmdb_apiv3key;
            app.dialog.preloader();
            app.request.json(url, function (data) {
                for (var i in data.results) {
                    data.results[i].image_path = tmdb_config.images.base_url + tmdb_config.images.poster_sizes[3] + data.results[i].backdrop_path;
                    data.results[i].stars = tmdb_stars(data.results[i], false);
                }
                app.dialog.close();
                content_cache[target] = movieList(data);
                app.$(target).empty().html(content_cache[target]);
                app.$('a.moviedetail').on('click', function (e) {
                    tmdb_moviedetails(app.$(this).data("movie"));
                });
            });
        } else {
            app.$(target).empty().html(content_cache[target]);
            app.$('a.moviedetail').on('click', function (e) {
                tmdb_moviedetails(app.$(this).data("movie"));
            });
        }
    } else {
        app.$(target).html('No network connection available.');
    }
}

/**
 * TMDB Search
 *
 * @param query
 */
function tmdb_search(query) {
    var opts = {
        api_key: tmdb_apiv3key,
        query: query
    };
    var url = tmdb_apiurl + '/search/movie?' + app.utils.serializeObject(opts);
    app.dialog.preloader("Searching...");
    app.request.json(url, function (data) {
        for (var i in data.results) {
            data.results[i].image_path = tmdb_config.images.base_url + tmdb_config.images.poster_sizes[3] + data.results[i].backdrop_path;
            data.results[i].stars = tmdb_stars(data.results[i], false);
        }
        app.dialog.close();
        app.$("#searchresults").empty().html(movieList(data));
        app.$('a.moviedetail').on('click', function (e) {
            tmdb_moviedetails(app.$(this).data("movie"));
        });
    });
}

/**
 * Show the Movie Detail popup
 * @param data
 */
function popup_details(data) {
    app.popup.create({
        content: '<div class="popup">' + data + "</div>",
    }).open();
}

/**
 * Handling event generated by cordova-plugin-network-information - detecting when we go offline
 */
app.$(document).on("offline", function (e) {
    isConnected = false;
});

/**
 * Reference to main view
 */
var mainView = null;

/**
 * Handling event generated by cordova-plugin-network-information - detecting when we go online
 */
app.$(document).on("online", function (e) {
    if (navigator.connection.type !== Connection.NONE) {
        app.dialog.close();
        tmdb_configure();
        if (mainView != null) {
            if (mainView.router.currentRoute.url == '/homepage') {
                mainView.router.refreshPage();
            } else {
                mainView.router.navigate("/homepage");
            }
        }
        isConnected = true;
    }
});

/**
 * creates the main view as soon as the device is ready
 */
app.$(document).on('deviceready', function (e) {
    mainView = app.views.create('.view-main', {
        url: '/homepage',
    });
    if (!isConnected) {
        app.dialog.preloader("Waiting network...");
    }
});