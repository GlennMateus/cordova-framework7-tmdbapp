# Cordova + Framework 7 TMDB Client

This is an academic project to evaluate Cordova and Framework 7's features.

## Cordova Network Information Plugin

This application relies on cordova-plugin-network-information (tested with version 2.0.1) to detect network changes.

## Framework 7

Version 2.1.3