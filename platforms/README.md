# Cordova Platforms

Here you find all added platforms to your application. Do not manage these files/directories by hand - use cordova cli instead.

To add a new platform, execute this command anywhere in the project's directories:

```bash
cordova platform add <platform>
```

To list all installed and available platforms, execute:

```bash
cordova platform list
```

To remove a platform, execute:

```bash
cordova platform rm <platform>
```